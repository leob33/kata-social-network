from cmd import Cmd
from typing import Any

import pyfiglet
from jinja2 import FileSystemLoader, Environment

from social_network.domain.ports.abstract_network_db import AbstractNetworkDB
from social_network.domain.user import User
from social_network.domain.user_exceptions import UserNotExistsError, SubscriptionError

import importlib_resources


class CmdPresenter(Cmd):
    prompt = f"(Network) ===>  "

    def __init__(self, user: User, network_db: AbstractNetworkDB):
        super().__init__()
        self.user = user
        self.network_db = network_db
        with importlib_resources.path('social_network.user_side', 'console_templates') as path:
            self.env = Environment(loader=FileSystemLoader(path))

    def cmdloop(self, intro: Any | None = ...) -> None:
        welcome_message = pyfiglet.figlet_format(f"Welcome to the Network {self.user.name}", font='slant')
        print(welcome_message)
        super().cmdloop()

    def do_home(self, args):
        template = self.env.get_template('home.txt')
        subscriptions = {name: "<===>".join(self.user.list_someone_messages(other_person_name=name))
                         for name in self.user.subscriptions}
        output = template.render(name=self.user.name,
                                 messages=self.user.messages,
                                 subscriptions=subscriptions,
                                 notifications=self.user.notifications)
        print(output)

    def do_post(self, message: str):
        try:
            self.user.add_message_to_timeline(message=message)
        except UserNotExistsError as error:
            print(error)

    def do_mymsg(self, args):
        for msg in self.user.messages:
            print(msg)

    def do_lmsg(self, other_person_name: str):
        try:
            for msg in self.user.list_someone_messages(other_person_name=other_person_name):
                print(msg)
        except UserNotExistsError as error:
            print(error)

    def do_subscribe(self, other_person_name: str):
        try:
            self.user.subscribe_to_someone_timeline(other_person_name=other_person_name)
        except (UserNotExistsError, SubscriptionError) as error:
            print(error)

    def do_dm(self, args: str):
        message, other_person_name = self._parsing_do_dm_input(args)
        try:
            self.user.send_dm(other_person_name=other_person_name, message=message)
        except UserNotExistsError as error:
            print(error)

    @staticmethod
    def _parsing_do_dm_input(user_input: str):
        parsing = user_input.split(' ')
        other_person_name = parsing[0]
        message = ' '.join(parsing[1:])
        return message, other_person_name

    def do_checkdm(self, other_person_name: str):
        try:
            list_tuples_message = self.user.check_dm(other_person_name=other_person_name)
            template = self.env.get_template('dm.txt')
            output = template.render(list_tuples_message=list_tuples_message)
            print(output)

        except UserNotExistsError as error:
            print(error)

    def do_exit(self, args):
        raise SystemExit()
