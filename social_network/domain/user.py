from social_network.domain.ports.abstract_network_db import AbstractNetworkDB
from social_network.domain.user_exceptions import UserExistsError, UserNotExistsError, SubscriptionError


class User:

    def __init__(self, name: str, network_db: AbstractNetworkDB):
        self.name = name
        self.network_db = network_db

    @property
    def notifications(self) -> list[str]:
        return self.network_db.list_notifications(user_name=self.name)

    @property  # to add them dynamically
    def messages(self) -> list[str]:
        self._ensure_user_registration(user_name=self.name)
        return self.network_db.list_messages_in_timeline(user_name=self.name)

    @property
    def subscriptions(self) -> list[str]:
        return self.network_db.list_subscriptions(user_name=self.name)

    def is_user_registered_to_network(self, user_name: str) -> bool:
        self.network_db.ensure_db_exists()
        return user_name in self.network_db.list_users()

    def register_to_network(self):
        self._ensure_user_registration(user_name=self.name, inverse=True)
        self.network_db.ensure_db_exists()
        self.network_db.register_user_to_network(user_name=self.name)

    def add_message_to_timeline(self, message: str) -> None:
        if '@' in message:
            mentions = list(filter(lambda word: word[0] == '@', message.split()))
            for mention in [mention.replace("@", '') for mention in mentions]:
                self._ensure_user_registration(user_name=mention)
                notification_msg = f'{self.name} mentioned you in {message}'
                self.network_db.update_notifications(user_name=mention, new_notification=notification_msg)
        self.network_db.add_message(message=message, user_name=self.name)

    def list_someone_messages(self, other_person_name: str):
        self._ensure_user_registration(user_name=other_person_name)
        return self.network_db.list_messages_in_timeline(user_name=other_person_name)

    def _ensure_user_registration(self, user_name: str, inverse: bool = False) -> None:
        if not inverse:
            if not self.is_user_registered_to_network(user_name=user_name):
                raise UserNotExistsError(f'{user_name} not registered to the network')
        else:
            if self.is_user_registered_to_network(user_name=user_name):
                raise UserExistsError(f'{user_name} is already registered to the network')

    def subscribe_to_someone_timeline(self, other_person_name: str):
        self._ensure_user_registration(user_name=other_person_name)
        if other_person_name in self.subscriptions:
            raise SubscriptionError(f"you have already subscribed to {other_person_name}'s timeline")
        if other_person_name == self.name:
            raise SubscriptionError(f"you cannot subscribe to your own timeline - does not make sense")
        self.network_db.update_subscriptions(user_name=self.name, followed=other_person_name)

    def send_dm(self, other_person_name: str, message: str):
        self._ensure_user_registration(user_name=other_person_name)
        self.network_db.send_dm(user_name=self.name, other_person_name=other_person_name, message=message)

    def check_dm(self, other_person_name: str):
        self._ensure_user_registration(other_person_name)
        return self.network_db.check_dm(user_name=self.name, other_person_name=other_person_name)

