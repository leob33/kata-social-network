class UserExistsError(Exception):
    pass


class UserNotExistsError(Exception):
    pass


class SubscriptionError(Exception):
    pass
