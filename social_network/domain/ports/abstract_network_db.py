from abc import ABCMeta, abstractmethod


class AbstractNetworkDB(metaclass=ABCMeta):

    @abstractmethod
    def add_message(self, message: str, user_name: str) -> None:
        raise NotImplementedError

    @abstractmethod
    def list_messages_in_timeline(self, user_name: str) -> list[str]:
        raise NotImplementedError

    @abstractmethod
    def list_users(self) -> list[str]:
        raise NotImplementedError

    @abstractmethod
    def register_user_to_network(self, user_name: str) -> None:
        raise NotImplementedError

    @abstractmethod
    def list_subscriptions(self, user_name: str) -> list[str]:
        raise NotImplementedError

    @abstractmethod
    def update_subscriptions(self, user_name: str, followed: str):
        raise NotImplementedError

    @abstractmethod
    def list_notifications(self, user_name: str) -> list[str]:
        raise NotImplementedError

    @abstractmethod
    def update_notifications(self, user_name: str, new_notification: str) -> None:
        raise NotImplementedError

    @abstractmethod
    def send_dm(self, user_name: str, other_person_name: str, message: str) -> None:
        raise NotImplementedError

    @abstractmethod
    def check_dm(self, user_name: str, other_person_name: str) -> None:
        raise NotImplementedError

    @abstractmethod
    def ensure_db_exists(self) -> None:
        raise NotImplementedError