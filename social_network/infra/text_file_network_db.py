import glob
from datetime import datetime
from pathlib import Path

from social_network.domain.ports.abstract_network_db import AbstractNetworkDB


class TextDBNotExistsError(Exception):
    pass


class TextDBExistsError(Exception):
    pass


class TextFileNetworkDB(AbstractNetworkDB):

    def ensure_db_exists(self) -> None:
        if not Path(self.db_directory).exists():
            raise TextDBNotExistsError(f'{self.db_directory} does not exists')

    def __init__(self, db_directory: str):
        self.db_directory = db_directory

    @staticmethod
    def create_empty_new_txt_file(filename: Path):
        if filename.exists():
            raise FileExistsError(f'{filename} already exists')
        with open(filename, 'a'):
            pass

    @staticmethod
    def append_line_to_txt_file(filename: Path, new_line: str):
        if not filename.exists():
            raise FileExistsError(f'{filename} does not exists')
        with open(filename, 'a') as file:
            file.write(new_line + '\n')

    @staticmethod
    def read_txt_content_as_list(filename: Path) -> list[str]:
        if not filename.exists():
            raise FileExistsError(f'{filename} does not exists')
        result = []
        with open(filename, 'r') as file:
            for line in file.readlines():
                result.append(line.replace('\n', ''))
        return result

    def list_subscriptions(self, user_name: str) -> list[str]:
        subscriptions_filename = Path(f'{self.db_directory}/{user_name}_subscriptions.txt')
        return self.read_txt_content_as_list(filename=subscriptions_filename)

    def update_subscriptions(self, user_name: str, followed: str):
        subscriptions_filename = Path(f'{self.db_directory}/{user_name}_subscriptions.txt')
        self.append_line_to_txt_file(filename=subscriptions_filename, new_line=followed)

    def list_notifications(self, user_name: str) -> list[str]:
        notifications_filename = Path(f'{self.db_directory}/{user_name}_notifications.txt')
        return self.read_txt_content_as_list(filename=notifications_filename)

    def update_notifications(self, user_name: str, new_notification: str) -> None:
        notifications_filename = Path(f'{self.db_directory}/{user_name}_notifications.txt')
        self.append_line_to_txt_file(filename=notifications_filename, new_line=new_notification)

    def register_user_to_network(self, user_name: str) -> None:
        for suffixe in ('.txt', '_subscriptions.txt', '_notifications.txt'):
            file = Path(f'{self.db_directory}/{user_name}{suffixe}')
            self.create_empty_new_txt_file(filename=file)

    def list_users(self):
        return [Path(path).stem for path in glob.glob(self.db_directory + '/' + '*.txt')]

    def add_message(self, message: str, user_name: str) -> None:
        db_filename = Path(f'{self.db_directory}/{user_name}.txt')
        self.append_line_to_txt_file(filename=db_filename, new_line=message)

    def list_messages_in_timeline(self, user_name: str) -> list[str]:
        db_filename = Path(f'{self.db_directory}/{user_name}.txt')
        return self.read_txt_content_as_list(filename=db_filename)

    def send_dm(self, user_name: str, other_person_name: str, message: str) -> None:
        dm_filename = Path(f'{self.db_directory}/{user_name}_to_{other_person_name}.txt')
        if not dm_filename.exists():
            self.create_empty_new_txt_file(filename=dm_filename)
        timestamp = datetime.now().strftime("%Y%m%d %H:%M:%S")
        dated_message = timestamp + '|' + message
        self.append_line_to_txt_file(filename=dm_filename, new_line=dated_message)

    def check_dm(self, user_name: str, other_person_name: str) -> list[tuple]:
        dm_filename_me_to_them = Path(f'{self.db_directory}/{user_name}_to_{other_person_name}.txt')
        dm_filename_them_to_me = Path(f'{self.db_directory}/{other_person_name}_to_{user_name}.txt')

        list_tuples_messages = []

        if dm_filename_me_to_them.exists():
            list_lines = self.read_txt_content_as_list(filename=dm_filename_me_to_them)
            list_tuples = [self._line_to_tuple(line=line, username='me') for line in list_lines]
            list_tuples_messages.extend(list_tuples)

        if dm_filename_them_to_me.exists() and user_name != other_person_name:
            list_lines = self.read_txt_content_as_list(filename=dm_filename_them_to_me)
            list_tuples = [self._line_to_tuple(line=line, username=other_person_name) for line in list_lines]
            list_tuples_messages.extend(list_tuples)

        list_tuples_messages = sorted(list_tuples_messages, key=lambda x: x[1])
        return list_tuples_messages

    def _line_to_tuple(self, line: str, username: str):
        [timestamp, message] = line.split('|')
        result = (username, timestamp, message)
        return result
