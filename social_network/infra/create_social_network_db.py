import shutil
from pathlib import Path

from social_network.infra.text_file_network_db import TextDBExistsError, TextDBNotExistsError


def create_social_network_db(db_directory: str, overwrite: bool) -> None:
    try:
        Path(db_directory).mkdir(exist_ok=overwrite)
    except FileExistsError:
        raise TextDBExistsError(f'{db_directory} already exists')


def delete_social_network_db(db_directory: str) -> None:
    try:
        shutil.rmtree(db_directory)
    except FileNotFoundError:
        raise TextDBNotExistsError(f'does not make sense : {db_directory} does not exists')