import click

from social_network.domain.user import User
from social_network.domain.user_exceptions import UserExistsError
from social_network.infra.create_social_network_db import create_social_network_db, delete_social_network_db
from social_network.infra.text_file_network_db import TextFileNetworkDB, TextDBExistsError, TextDBNotExistsError
from social_network.user_side.cmd_presenter import CmdPresenter

DB_DIRECTORY = 'network_db'


@click.group()
def cli_entry_point():
    pass


@cli_entry_point.command()
def init():
    try:
        create_social_network_db(db_directory=DB_DIRECTORY, overwrite=False)
    except TextDBExistsError as error:
        print(error)


@cli_entry_point.command()
def rm():
    try:
        delete_social_network_db(db_directory=DB_DIRECTORY)
    except TextDBNotExistsError as error:
        print(error)


@cli_entry_point.command()
@click.argument('name')
def connect(name):
    network_db = TextFileNetworkDB(db_directory=DB_DIRECTORY)
    user = User(name=name, network_db=network_db)
    try:
        if user.is_user_registered_to_network(user_name=user.name):
            cmd_line_app = CmdPresenter(user=user, network_db=network_db)
            cmd_line_app.cmdloop()
        else:
            print(f"{user.name} needs to register to network first")
    except TextDBNotExistsError as error:
        print(error)


@cli_entry_point.command()
@click.argument('name')
def register(name):
    network_db = TextFileNetworkDB(db_directory=DB_DIRECTORY)
    user = User(name=name, network_db=network_db)
    try:
        user.register_to_network()
    except (TextDBNotExistsError, UserExistsError) as error:
        print(error)
