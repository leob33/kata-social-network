TEST_FOLDER = tests
SRC_FOLDER = social_network


# +++++++++ PACKAGING ++++++++

build-wheel:
	pip install build
	python -m build --wheel

install-wheel: build-wheel
	pip install dist/*.whl


# ================= OTHER USEFUL COMMAND =================

install-package-devmode: build-wheel
	pip install -e '.[dev]' # install extra labelled dev dependencies

clean:
	rm -rf build dist .pytest_cache $(SRC_FOLDER).egg-info reports .coverage

uninstall-all:
	pip freeze | grep -v "@" | xargs pip uninstall -y
	pip uninstall $(SRC_FOLDER) -y