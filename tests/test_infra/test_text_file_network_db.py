import pytest

from social_network.infra.create_social_network_db import create_social_network_db, delete_social_network_db
from social_network.infra.text_file_network_db import TextFileNetworkDB

DB_DIRECTORY = 'test_social_network_db'
USER = 'test_user'


@pytest.fixture
def text_file_social_network():
    create_social_network_db(db_directory=DB_DIRECTORY, overwrite=True)
    text_file_social_network = TextFileNetworkDB(db_directory=DB_DIRECTORY)
    yield text_file_social_network
    delete_social_network_db(db_directory=DB_DIRECTORY)


def test_register_user_to_network(text_file_social_network):
    text_file_social_network.register_user_to_network(user_name=USER)

    assert USER in text_file_social_network.list_users()


def test_register_user_to_network_given_unknown_user(text_file_social_network):
    assert USER not in text_file_social_network.list_users()


def test_add_message_given_unknown_user(text_file_social_network):
    with pytest.raises(FileExistsError):
        text_file_social_network.add_message(message='nothing', user_name=USER)


def test_add_message_given_known_user(text_file_social_network):
    text_file_social_network.register_user_to_network(user_name=USER)

    text_file_social_network.add_message(message='nothing', user_name=USER)

    assert ['nothing'] == text_file_social_network.list_messages_in_timeline(user_name=USER)


def test_list_messages_in_timeline_given_unknown_user(text_file_social_network):
    with pytest.raises(FileExistsError):
        text_file_social_network.list_messages_in_timeline(user_name=USER)
