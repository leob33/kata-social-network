from unittest.mock import Mock, patch

import pytest

from social_network.domain.user_exceptions import UserExistsError, UserNotExistsError, SubscriptionError
from social_network.domain.ports.abstract_network_db import AbstractNetworkDB
from social_network.domain.user import User


def test_is_user_registered_to_network_returns_true_given_user_in_network():
    """test_<function_to_test>_<expected_behavior>_given_<context>"""
    mocked_timeline = Mock(AbstractNetworkDB)
    mocked_timeline.list_users.return_value = ['leo', 'nathan']
    nathan = User(name='nathan', network_db=mocked_timeline)

    assert nathan.is_user_registered_to_network(user_name=nathan.name)


def test_is_user_registered_to_network_returns_false_given_user_not_in_network():
    mocked_timeline = Mock(AbstractNetworkDB)
    mocked_timeline.list_users.return_value = ['leo']
    nathan = User(name='nathan', network_db=mocked_timeline)

    assert not nathan.is_user_registered_to_network(user_name=nathan.name)


def test_register_to_network_raises_network_exception_given_user_already_in_network():
    mocked_timeline = Mock(AbstractNetworkDB)
    mocked_timeline.list_users.return_value = ['leo', 'nathan']
    nathan = User(name='nathan', network_db=mocked_timeline)

    with pytest.raises(UserExistsError):
        nathan.register_to_network()


def test_register_to_network_calls_register_user_to_network_exception_given_user_already_in_network():
    mocked_timeline = Mock(AbstractNetworkDB)
    mocked_timeline.list_users.return_value = ['leo']
    nathan = User(name='nathan', network_db=mocked_timeline)

    nathan.register_to_network()

    mocked_timeline.register_user_to_network.assert_called_with(user_name='nathan')


def test_add_to_timeline_calls_add_message_called_when_nathan_add_message_to_timeline():
    mocked_timeline = Mock(AbstractNetworkDB)
    mocked_timeline.list_users.return_value = ['nathan']
    nathan = User(name='nathan', network_db=mocked_timeline)
    message = 'hello'

    nathan.add_message_to_timeline(message=message)

    mocked_timeline.add_message.assert_called_with(message=message, user_name=nathan.name)


def test_messages_calls_list_messages_in_timeline_given_user_registered_to_network():
    mocked_timeline = Mock(AbstractNetworkDB)
    nathan = User(name='nathan', network_db=mocked_timeline)

    # on peut aussi utilisé mocked timeline
    with patch.object(User, "is_user_registered_to_network") as is_user_registered_to_network_mocked:
        is_user_registered_to_network_mocked.return_value = True
        nathan.messages

    mocked_timeline.list_messages_in_timeline.assert_called_with(user_name='nathan')


def test_messages_raises_network_exception_given_user_not_registered_to_network():
    mocked_timeline = Mock(AbstractNetworkDB)
    nathan = User(name='nathan', network_db=mocked_timeline)

    with pytest.raises(UserNotExistsError):
        with patch.object(User, "is_user_registered_to_network") as is_user_registered_to_network_mocked:
            is_user_registered_to_network_mocked.return_value = False
            nathan.messages


def test_list_someone_messages_calls_list_messages_in_timeline_given_both_users_registered_to_network():
    mocked_timeline = Mock(AbstractNetworkDB)
    nathan = User(name='nathan', network_db=mocked_timeline)
    other_user = 'other_user'

    with patch.object(User, "is_user_registered_to_network") as is_user_registered_to_network_mocked:
        is_user_registered_to_network_mocked.return_value = True
        nathan.list_someone_messages(other_person_name=other_user)

    mocked_timeline.list_messages_in_timeline.assert_called_with(user_name=other_user)


def test_list_someone_messages_raises_network_exception_given_user_not_registered_to_network():
    mocked_timeline = Mock(AbstractNetworkDB)
    nathan = User(name='nathan', network_db=mocked_timeline)
    leo = 'leo'

    with pytest.raises(UserNotExistsError):
        with patch.object(User, "is_user_registered_to_network") as is_user_registered_to_network_mocked:
            is_user_registered_to_network_mocked.return_value = False
            nathan.list_someone_messages(other_person_name=leo)
